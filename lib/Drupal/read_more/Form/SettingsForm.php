<?php

/**
 * @file
 * Contains \Drupal\read_more\Form\SettingsForm.
 */

namespace Drupal\read_more\Form;

use Drupal\Core\Form\ConfigFormBase;

class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'read_more_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    
    $config = $this->configFactory->get('read_more.settings');

    $elements = array(
      'address' => '<address>',
      'blockquote' => '<blockquote>',
      'cite' => '<cite>',
      'div' => '<div>',
      'h1' => '<h1>',
      'h2' => '<h2>',
      'h3' => '<h3>',
      'h4' => '<h4>',
      'h5' => '<h5>',
      'h6' => '<h6>',
      'p' => '<p>',
      'span' => '<span>',
    );

    $form['read_more_behavior'] = array(
      '#type' => 'details',
      '#title' => t('Link behavior'),
      '#open' => TRUE,
    );
    $form['read_more_behavior']['read_more_placement'] = array(
      '#type' => 'radios',
      '#title' => t('Link placement'),
      '#options' => array(
        'inline' => t('Inline: Try to add the Read More link after the last word of the teaser. If this fails, add the link on a new line after the teaser.'),
        'after' => t('On a new line: Add the Read More link on a new line after the teaser.'),
        'disable' => t('Disable the link: Do not add a Read More link to the teaser.'),
      ),
      '#default_value' => $config->get('read_more_placement'),
      '#description' => t('The inline option will attempt to add the Read More link after the last word of the teaser and before any CCK fields. The HTML elements into which the Read More link may be inserted can be chosen in the "Advanced options for inline placement" interface below.'),
    );
    $form['read_more_behavior']['read_more_placement_advanced'] = array(
      '#type' => 'details',
      '#title' => t('Advanced options for inline placement'),
      '#open' => FALSE,
    );
    $form['read_more_behavior']['read_more_placement_advanced']['read_more_elements'] = array(
      '#type' => 'select',
      '#title' => t('Elements eligible for inline placement'),
      '#description' => t('Select the elements into which the Read More link may be inserted. The "Inline" placement option must be selected above.'),
      '#multiple' => TRUE,
      '#options' => $elements,
      '#default_value' => $config->get('read_more_elements'),
      '#size' => 10,
    );
    $form['read_more_behavior']['read_more_remove'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove Read More link from links section'),
      '#default_value' => $config->get('read_more_remove'),
      '#description' => t('Enabling this option will remove Drupal\'s default Read More link from the node links.'),
    );
    $form['read_more_behavior']['read_more_rss'] = array(
      '#type' => 'checkbox',
      '#title' => t('Replace Read More link in RSS feeds'),
      '#default_value' => $config->get('read_more_rss'),
    );
    $form['read_more_behavior']['read_more_require_body_field'] = array(
      '#type' => 'checkbox',
      '#title' => t('Do not display if the body text is empty'),
      '#default_value' => $config->get('read_more_require_body_field'),
    );
    $form['read_more_behavior']['read_more_anchor'] = array(
      '#type' => 'checkbox',
      '#title' => t('Skip to unread content (SEE WARNING BELOW)'),
      '#default_value' => $config->get('read_more_anchor'),
      '#description' => t('Enabling this option will add an anchor to the destination page so that the user skips past the content they already saw in the teaser. WARNING: This feature is still being tested! Please report any problems you experience in the <a href="@link">Read More issue queue</a>.', array('@link' => url('http://drupal.org/project/issues/read_more'))),
    );

    $form['read_more_formatting'] = array(
      '#type' => 'details',
      '#title' => t('Link text and formatting'),
      '#description' => t('Here you can specify the wording of the Read More link, change its appearance by wrapping it in markup, and use tokens to display information about the node.'),
      '#open' => TRUE,
    );
    $form['read_more_formatting']['read_more_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => $config->get('read_more_text'),
      '#description' => t('Enter the text you wish to display in the Read More link. Special characters should be encoded (like <code>&amp;raquo;</code> or <code>&amp;amp;</code>). Allowed HTML is listed below.'),
      '#required' => TRUE,
    );
    $form['read_more_formatting']['allowed_html'] = array(
      '#type' => 'details',
      '#title' => t('Allowed HTML'),
      '#open' => FALSE,
      '#value' => t('The following HTML is allowed in the link text field above:') . ' <code>abbr</code>, <code>acronym</code>, <code>b</code>, <code>big</code>, <code>cite</code>, <code>code</code>, <code>del</code>, <code>em</code>, <code>i</code>, <code>img</code>, <code>ins</code>, <code>small</code>, <code>span</code>, <code>strong</code>, <code>sub</code>, <code>sup</code>',
    );
    $form['read_more_formatting']['read_more_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Link title attribute'),
      '#default_value' => $config->get('read_more_title'),
      '#description' => t('Enter the text you wish to be used as the title for the Read More link (the value of the <code>title=""</code> attribute). The link title is used for accessibility and search engine optimization and appears as a tooltip in some browsers.'),
      '#required' => FALSE,
    );
    $form['read_more_formatting']['read_more_nofollow'] = array(
      '#type' => 'checkbox',
      '#title' => t('Make link nofollow'),
      '#default_value' => $config->get('read_more_nofollow'),
      '#description' => t('Adds <code>rel="nofollow"</code> to the link\'s attributes. Often used for search engine optimization.'),
    );
    $form['read_more_formatting']['read_more_newwindow'] = array(
      '#type' => 'checkbox',
      '#title' => t('Make link open in a new window'),
      '#default_value' => $config->get('read_more_newwindow'),
      '#description' => t('Adds <code>target="_blank"</code> to the link\'s attributes.'),
    );

    // Add help text for tokens and list of tokens if the Tokens module is enabled.
    $token_description = t('Use the <code>[node:title]</code> token to insert the node title.');
    if (module_exists('token')) {
      $token_description = t('Other available <a href="@tokens">tokens</a> are listed below.', array('@tokens' => url('http://drupal.org/project/token')));
      $form['read_more_tokens'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Tokens'),
        '#description' => t('The following tokens can be used in the Read More link text and title attribute.'),
      );
      $form['read_more_tokens']['list'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('node'),
      );
    }
    $form['read_more_formatting']['read_more_text']['#description'] .= ' ' . $token_description;
    $form['read_more_formatting']['read_more_title']['#description'] .= ' ' . $token_description;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    
    $config = $this->configFactory->get('read_more.settings');

    $config->set('read_more_placement', $form_state['values']['read_more_placement'])
        ->set('read_more_text', $form_state['values']['read_more_text'])
        ->set('read_more_elements', $form_state['values']['read_more_elements'])
        ->set('read_more_remove', $form_state['values']['read_more_remove'])
        ->set('read_more_rss', $form_state['values']['read_more_rss'])
        ->set('read_more_nofollow', $form_state['values']['read_more_nofollow'])
        ->set('read_more_require_body_field',$form_state['values']['read_more_require_body_field'])
        ->set('read_more_anchor',$form_state['values']['read_more_anchor'])
        ->set('read_more_title',$form_state['values']['read_more_title'])
        ->set('read_more_newwindow',$form_state['values']['read_more_newwindow'])
        ->save();
    parent::submitForm($form, $form_state);
    
    
  }

}