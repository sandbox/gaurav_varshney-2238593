
=====
Read More Link
http://drupal.org/project/read_more
-----
This module allows you to move the Read More link from the node's links area to the end of the teaser text.


=====
Styling the Read More link
-----

You can style the Read More link by adding the following to your theme's stylesheet (usually style.css):

div.read-more,
span.read-more {
  /* Whatever style you want here */
  }

Remember to clear your cache if you make any changes to your theme's stylesheet.


=====
Credits
-----
The methods employed in this module were originally described in this blog post:
http://www.angrydonuts.com/the_nuisance_of_the_read_more_fl
